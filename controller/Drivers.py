import os
import json

class Drivers():
    def __init__(self,city):
        self.city=city

    def updateLocation(self,driversLocation):
        if driversLocation!='':
            locationDetailsJson=eval(driversLocation)
            allDetails=locationDetailsJson["Details"]
            for detail in allDetails:
                driverCode=detail['driverCode']
                print(detail)
                data={
                    "driverCode":detail['driverCode'],
                    "Latitude":detail['Latitude'],
                    "Longitude": detail['Longitude']
                }
                jsonData = json.dumps(data, indent=3)
                # f = open('./Metadata/'+self.city+'/DriverLocation/'+driverCode+'.txt', "w")
                f = open('D:\\QACreateCode\\Metadata\\DefaultCityCode\\DriverLocation\\' + driverCode + '.txt', "w")
                f.write(jsonData)
                f.close()

    def updateBooked(self,bookedDriverseCode,bookingTime,bookingLatitude,bookingLongitude):
        node={
                "driverCode": bookedDriverseCode,
                "BookingTime":bookingTime,
                "LatitudePickup":bookingLatitude,
                "LongitutePickup":bookingLongitude
            }
        jsonData = json.dumps(node, indent=3)
        # f = open('./Metadata/' + self.city + '/BusyDrivers/' + bookedDriverseCode + '.txt', "w")
        f = open('D:\\QACreateCode\\Metadata\\DefaultCityCode\\BusyDrivers\\' + bookedDriverseCode + '.txt', "w")
        f.write(jsonData)
        f.close()

    def bookingComplete(self,bookedDriverseCode):
        # os.remove('./Metadata/' + self.city + '/BusyDrivers/' + bookedDriverseCode + '.txt')
        os.remove('D:\\QACreateCode\\Metadata\\DefaultCityCode\\BusyDrivers\\' + bookedDriverseCode + '.txt')

if __name__ == '__main__':
    PU= Drivers('DefaultCityCode')
    jsonV={"Details":[
        {
            "driverCode": "A100",
            "Latitude": "15",
            "Longitude":"5"
        }
    ]}
    PU.updateLocation(json.dumps(jsonV))
    PU.updateBooked('A100','','','')
    PU.bookingComplete('A100')






