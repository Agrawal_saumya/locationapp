This is a file based system.
How to run the code :
1. Deploy this code in D: drive under folder name ''
2. The webservice APIs are provide in webservice details document.
3. Data in the file systrm can be backed in database from time to time by any scheduled process.
4.File Based system is chosrn to increase performance and provide flexibility.
5. Currently the code assumes that the location coordinates are based on metre graph based points.
6. The metadata in the code contains all the drivers data ,their location and availability.
7. While providing list of drivers present in 200 metres radius, the code keeps in mind an extra factor of driver's availability 
8. A driver is available only if he is not in mid of any ride.
9. Thus the drivers that are present in 200metres radius are the drivers present- non available drivers.
Few metadata files have already added in system with Drivercode: A100,A103,A105 etc.
