#  QASalesCrediting/QASalesCrediting.py
#  Copyright (C) 2019 Axtria.
#  Execute Methods related to Templatization Module of SalesCrediting and DataProcessing
#  Author: Saumya Agrawal <saumya.agrawal@axtria.com>

from flask import Flask, request
import json
import logging
import configparser
from logging.handlers import TimedRotatingFileHandler
from controller.Drivers import Drivers
from services.Availability import calculateAvailability
app = Flask(__name__)

test_config = configparser.ConfigParser()
test_config._interpolation = configparser.ExtendedInterpolation()
test_config.optionxform = str
test_config.read('./Static_Config/config.ini')
ServicePrefix=test_config['web_service']['prefix_run_execution']

log='./Logs/log.txt'
logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler(log, when="midnight"),
                              logging.StreamHandler()],
                    format='%(asctime)s - %(levelname)s - %(message)s')

@app.route(ServicePrefix+"/")
def Hello():
    logging.debug('helo')
    return ServicePrefix +' webservice running'

@app.route(ServicePrefix+"/trackLocation/", methods=['POST','GET'])
def trackLocation():
    try:
        logging.debug("----in trackLocation--------------------")
        cityCode = request.form['cityCode']
        driversLocation=request.form['driversLocation']
        D=Drivers(cityCode)
        D.updateLocation(driversLocation)
        return 'Success'
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        logging.debug(e)
        return 'Failed'

@app.route(ServicePrefix+"/findAvailability/", methods=['POST','GET'])
def findAvailability():
    try:
        logging.debug("----in findAvailability--------------------")
        cityCode = request.form['cityCode']
        latitude=request.form['latitude']
        longitude=request.form('longitude')
        drivers=calculateAvailability(cityCode,latitude,longitude)
        return drivers
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        logging.debug(e)
        return 'Failed'



if __name__ == '__main__':
	app.run(debug=True)










