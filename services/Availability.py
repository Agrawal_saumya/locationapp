import os
import json
from os import path

def calculateAvailability(cityCode,latitudeC,longitudeC):
    r=200
    withInRadiusDrivers=[]
    # for r, d, f in os.walk('./Metadata/'+cityCode+'/DriverLocation'):
    for r, d, f in os.walk('D:\\QACreateCode\\Metadata\\DefaultCityCode\\DriverLocation'):
        for file in f:
            driverCode=file.split('.txt')[0]
            # if path.exists('./Metadata/'+cityCode+'/BusyDrivers/'+driverCode+'.txt')==False:
            if path.exists( 'D:\\QACreateCode\\Metadata\\DefaultCityCode\\BusyDrivers\\'+ driverCode + '.txt') == False:
                latitudeD, longitudeD,driverData =returnDriversLocation(r+'\\'+file)
                if distance(float(latitudeD),float(latitudeC),float(longitudeC),float(longitudeD))<=200:
                    withInRadiusDrivers.append(driverCode)

    return json.dumps({"Drivers":withInRadiusDrivers})

def returnDriversLocation(location):
    with open(location) as data:
        driverData = json.load(data)
        latitudeD = float(driverData['Latitude'])
        longitudeD = float(driverData['Longitude'])
        driverCode=driverData['driverCode']
        return latitudeD,longitudeD,driverCode

def distance(x,y,xo,yo):
    return pow(x - xo, 2) + pow(y - yo, 2)
    print

if __name__ == '__main__':
    listD=calculateAvailability('DefaultCityCode','2','3')
    print(listD)